%define NEXT 0

%macro colon 2
%ifnstr %1
        %error "Ключ макроса должен быть строкой!"
%endif
%ifnid %2
        %error "Второй аргумент макроса должен быть меткой!"
%endif
%2:
        dq NEXT
        db %1
        db 0

%define NEXT %2

%endmacro
