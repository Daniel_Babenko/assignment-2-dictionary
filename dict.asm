%include "lib.inc"
%define NEXT_BYTE 8
global find_word

find_word:
        push    r12
.loop:
        mov     r12, rsi
        lea     rsi, [rsi+NEXT_BYTE]
        call    string_equals
        test    rax, rax
        jz      .next
        mov     rax, r12
        pop     r12
        ret
.next:
        mov     rsi, r12
        mov     rsi, [rsi]
        test    rsi, rsi
        jnz     .loop
        xor     rax, rax
        pop     r12
        ret
