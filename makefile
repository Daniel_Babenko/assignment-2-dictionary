LD=ld
ASM=nasm
ASMFLAGS=-f elf64
PROGRAM=main
SRC=main.o lib.o dict.o

main.o: main.asm lib.inc words.inc dict.inc
dict.o: dict.asm lib.inc
words.inc: colon.inc

%.o: %.asm
        $(ASM) $(ASMFLAGS) -o $@ $<

$(PROGRAM): $(SRC)
        $(LD) -o $@ $+

.PHONY: clean test

test: $(PROGRAM)
        python3 test.py

clean:
        rm -f $(PROGRAM) $(SRC)
