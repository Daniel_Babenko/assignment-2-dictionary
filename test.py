from subprocess import Popen, PIPE

inputs = ["Zerumi", "Decaf", "Abobus", "Kamilchik", "abobus", "MORTIUM", "This_String_Is_Meant_To_Be_Very_Long_So_I_Can_Check_If_Throwing_An_Exception_Of_Having_More_Than_256_Symbols_Works_Because_In_Our_Precious_University_We_Want_To_Develop_Our_Little_Labworks_Like_They_Are_Enormous_And_Important_Projects_And_We_Really_Care_For_The_Results_Of_Them_And_Wish_They_Will_Grow_Into_Something_Meaningfull_In_The_Nearest_Future__Oh_If_You_Have_Just_Spent_Your_Time_On_Reading_This_I_Wish_Good_Luck_For_You_And_Your_Family__Regards_Daniel"]

outputs = ["Кирилл Афанасьев", "Александр Разинкин", "AMONG US!!!","Я не мёртвый, я живой ()_()", "","", ""]
errors = ["", "", "", "", "Слово не найдено!", "Слово не найдено!", "Слишком длинное слово!"]

for i in range(len(inputs)):
  p = Popen(["./main"],
        stdin=PIPE,
        stdout=PIPE,
        stderr=PIPE)
  inp = inputs[i]
  out = outputs[i]
  err = errors[i]
  data = p.communicate(inp.encode())
  if data[0].decode().strip() == out and data[1].decode().strip() == err:
    print("Тест # " + str(i+1) + " прошёл  успешно с входными данными: \"" + inp + "\"")
    if inp == "MORTIUM":
        print("P.S. Мартин Вальц лучший!")
  else:
    print("Тесты не пройдены: \"" + data[0].decode().strip() + "\", stderr: \"" + data[1].decode().strip() + "\". Ожидаемый ответ: \"" + out + "\", stderr: \"" + err + "\"")
