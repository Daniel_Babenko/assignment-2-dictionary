%include "dict.inc"
%include "lib.inc"
%include "words.inc"

%define BUFFER_SIZE 256
%define LIMIT 255
%define WRITE_SYSCALL 1
%define ERR_FD 2

section .bss
BUF:
resb BUFFER_SIZE

section .rodata
too_long_message:
        db "Слишком длинное слово!", 10, 0
not_found_message:
        db "Слово не найдено!", 10, 0

section .text
global _start

_start:
        mov     rdi, BUF
        mov     rsi, LIMIT
        call    read_word
        test    rax, rax
        jz      .too_long_error
        mov     rsi, first_word
        mov     rdi, BUF
        call    find_word
        test    rax, rax
        jz      .not_found_error
        mov     rdi, rax
        add     rdi, 8
        push    rdi
        call    string_length
        pop     rdi
        add     rdi, rax
        inc     rdi
        call    print_string
        call    print_newline
        xor     rdi, rdi
        jmp    exit

.too_long_error:
        mov     rdi, too_long_message
        jmp     .print_error_message

.not_found_error:
        mov     rdi, not_found_message

.print_error_message:
        push    rdi
        call    string_length
        pop     rsi
        mov     rdx, rax
        mov     rax, WRITE_SYSCALL
        mov     rdi, ERR_FD
        syscall
        jmp    exit
